import os
from groq import Groq
import discord
from dotenv import load_dotenv
from pypdf import PdfReader
import tqdm
import logging
from icecream import ic
import tempfile
import chromadb
import requests
from chromadb.utils import embedding_functions as ef

ELEM_PATH = "https://hastie.su.domains/Papers/ESLII.pdf"
SMOLA_PATH = "https://d2l.ai/d2l-en.pdf"

logging.basicConfig(level=logging.INFO)
log_format = "[%(levelname)s]: %(message)s"
logger = logging.getLogger(__name__)


def info(s):
    logger.info(s)


ic.configureOutput(outputFunction=info)

load_dotenv()

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)
groq_client = Groq(api_key=os.getenv("GROQ_API"))


def load_pdf(pdf_file_path):
    logger.info("reading pdf:")
    ic(pdf_file_path)
    with open(pdf_file_path, "rb") as f:
        reader = PdfReader(f)
        text = ""
        for page in tqdm.tqdm(reader.pages):
            text += page.extract_text()
        return text


def download_pdf(url, temp_dir=None):
    try:
        # Download the PDF file
        response = requests.get(url, stream=True)
        response.raise_for_status()

        # Create a temporary file
        if temp_dir:
            temp_file = tempfile.NamedTemporaryFile(
                dir=temp_dir, delete=False, suffix=".pdf"
            )
        else:
            temp_file = tempfile.NamedTemporaryFile(delete=False, suffix=".pdf")

        # Write the PDF data to the temporary file
        with temp_file as f:
            for chunk in response.iter_content(chunk_size=8192):
                f.write(chunk)

        logger.info(f"PDF downloaded and saved to {temp_file.name}")
        return temp_file.name

    except requests.exceptions.RequestException as e:
        logger.error(f"Error downloading PDF: {e}")
    except Exception as e:
        logger.error(f"Unexpected error: {e}")


def load_pdf_from_url(url):
    with tempfile.TemporaryDirectory() as dir:
        pdf_path = download_pdf(url, dir)
        return load_pdf(pdf_path)


def initialize_vector_db(force=False):
    client = chromadb.PersistentClient(path="store/textbooks")
    textbooks = client.get_or_create_collection("textbooks")
    embedder = ef.DefaultEmbeddingFunction()

    if force or textbooks.count() == 0:
        client.delete_collection(name="textbooks")
        textbooks = client.create_collection("textbooks")
        for textbook_path in [ELEM_PATH, SMOLA_PATH]:
            text = load_pdf_from_url(textbook_path)
            textbooks.add(documents = [text])

    return textbooks

__import__('pdb').set_trace()

@client.event
async def on_ready():
    print(f"we have logged in as {client.user}")


@client.event
async def on_message(message):
    print(message.author)
    if message.author == client.user:
        return
    if message.content.startswith("$bot"):
        if type(message.channel) == discord.channel.TextChannel:
            async with message.channel.typing():
                proc_message = message.content.split("$bot ")[1]
                if str(message.author) == "nixon0.0":
                    chat_completion = groq_client.chat.completions.create(
                        messages=[
                            {
                                "role": "system",
                                "content": "You are a vulgar pirate. Act as vulgar as possible.",
                            },
                            {"role": "user", "content": proc_message},
                        ],
                        model="mixtral-8x7b-32768",
                        temperature=0.6,
                    )
                else:
                    message.channel = await message.create_thread(name=proc_message)

                    print(type(message.channel))
                    chat_completion = groq_client.chat.completions.create(
                        messages=[{"role": "user", "content": proc_message}],
                        model="mixtral-8x7b-32768",
                    )
                    response = chat_completion.choices[0].message.content
                    if len(response) > 1900:
                        response = response[:1900]
                    print(response)
                    await message.channel.send(f"{response}")
        elif type(message.channel) == discord.threads.Thread:
            print(message.channel.starter_message.content)
            messages = [
                message
                async for message in message.channel.history(
                    limit=25, oldest_first=True
                )
            ]
            full_mssg = []
            for idx, val in enumerate(messages):
                if str(val.author) != "Test_Bot#0825":
                    if val.content.startswith("$bot "):
                        proc_message = val.content.split("$bot ")[1]
                        new_dict = {"role": "user", "content": proc_message}
                        full_mssg.append(new_dict)
                    else:
                        pass  # question whether we should add non $bot messages to llm mssg history
                else:
                    new_dict = {"role": "assistant", "content": val.content}
                    full_mssg.append(new_dict)
            full_mssg[0] = {
                "role": "user",
                "content": message.channel.starter_message.content.split("$bot ")[1],
            }
            print(full_mssg)

            chat_completion = groq_client.chat.completions.create(
                messages=full_mssg,
                model="mixtral-8x7b-32768",
            )
            response = chat_completion.choices[0].message.content
            if len(response) > 1900:
                response = response[:1900]
            print(response)
            await message.channel.send(f"{response}")


client.run(os.getenv("DISCORD_TOKEN"))

# GUILD = "TestBot"

# @client.event
# async def on_ready():
#     for guild in client.guilds:
#         if guild.name == GUILD:
#             break

#     print(
#         f"{client.user} is connected to the following guild:\n"
#         f"{guild.name}(id: {guild.id})"
#     )

# client.run(os.getenv("DISCORD_TOKEN"))
